function duDoan() {
  var canh1 = document.getElementById("canhthu1").value * 1;
  var canh2 = document.getElementById("canhthu2").value * 1;
  var canh3 = document.getElementById("canhthu3").value * 1;
  if (canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh3 + canh2 > canh1) {
    if (canh1 == canh2 && canh1 == canh3 && canh2 == canh3) {
      document.getElementById("result").innerText = `Hình tam giác đều `;
    } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
      document.getElementById("result").innerText = `Hình tam giác cân `;
    } else if (canh3 * canh3 == canh1 * canh1 + canh2 * canh2) {
      document.getElementById("result").innerText = `Hình tam giác vuông `;
    } else {
      document.getElementById("result").innerText = `Hình tam giác khác `;
    }
  } else {
    document.getElementById("result").innerText = `Không hợp lệ `;
  }
}
//input: độ dài 3 cạnh của tam giác
//các bước xử lí
//  bước 1: khai báo biến canh1,canh2,canh3 ứng với 3 cạnh của tam giác
//  bước 2:gán giá trị cho canh1,canh2,canh3
//  bước 3: xét điều kiện để là 3 cạnh của tam giác là canh1 + canh2 > canh3 && canh1 + canh3 > canh2 && canh3 + canh2 > canh1 nêus ko thỏa thì ia ra ko hợp lệ
//  bước 4: xét điều kiện là tam giác đều là   canh1 === canh2 === canh3 và in ra là Hình tam giác đều
//  bước 5: xét điều kiện là tam giác cân là   canh1 == canh2 || canh1 == canh3 || canh2 == canh3 và in ra là Hình tam giác cân
//  bước 6: xét điều kiện là tam giác vuông là canh3 * canh3 == canh1 * canh1 + canh2 * canh2 và in ra là Hình tam giác vuông
//  bước 7: nếu ko thỏa các điều kiện tam giác trên thì in ra là  Hình tam giác khác
//output: lạo Hình tam giác được dự đoán

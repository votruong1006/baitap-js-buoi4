function sapXepSo() {
  var so1 = document.getElementById("sothu1").value * 1;
  var so2 = document.getElementById("sothu2").value * 1;
  var so3 = document.getElementById("sothu3").value * 1;
  if (so1 > so2 && so1 > so3) {
    if (so2 > so3) {
      document.getElementById("result").innerText = `${so3} , ${so2} , ${so1}`;
    } else {
      document.getElementById("result").innerText = `${so2} , ${so3} , ${so1}`;
    }
  } else if (so2 > so1 && so2 > so3) {
    if (so1 > so3) {
      document.getElementById("result").innerText = `${so3} , ${so1} , ${so2}`;
    } else {
      document.getElementById("result").innerText = `${so1} , ${so3} , ${so2}`;
    }
  } else if (so3 > so1 && so3 > so2) {
    if (so1 > so2) {
      document.getElementById("result").innerText = `${so2} , ${so1} , ${so3}`;
    } else {
      document.getElementById("result").innerText = `${so1} , ${so2} , ${so3}`;
    }
  } else {
    document.getElementById("result").innerText = `${so1} , ${so2} , ${so3}`;
  }
}
//input: 3 số người dùng nhập vào
//các bước xử lí:
//    b1:khai báo 3 biến so1, so2, so3 ứng với 3 con số người dùng nhập vào
//    b2:gán giá trị cho  so1,so2,so3
//    b3:sử dụng lệnh if-else-if để so sánh giá trị của so1,so2,so3
//    b4:sử dụng lệnh if thứ 1 với điều kiện  so1>so2 && so1>so3 trong lệnh này ta có 1 lệnh if-else nhỏ để so sánh so2 và so3 là so2>so3 ta được stt là so3,so2,so1 và ngược lại ta có stt so2,so3,so1
//    b5:tiếp theo sử dụng lệnh else-if với điều kiện  so2 > so1 && so2 > so3 trong lệnh này ta có 1 lệnh if-else nhỏ để so sánh so1 và so3 là so1>so3 ta được stt là so3,so1,so2 và ngược lại ta có stt so1,so3,so2
//    b6:tiếp theo sử dụng lệnh else-if với điều kiện  so3 > so1 && so3 > so2 trong lệnh này ta có 1 lệnh if-else nhỏ để so sánh so1 và so2 là so1>so2 ta được stt là so2,so1,so3 và ngược lại ta có stt so1,so2,so3
//    b7:cuối cùng ta dùng lệnh else để đưa ra stt khi 3 số bằng nhau là so1,so2,so3
//    b8:xuất ra màn hình với stt tương ứng với 3 con số người dùng nhap vao
//output: số thứ tự từ nhỏ tới lớn của 3 số vừa nhập

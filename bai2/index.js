function guiLoiChao() {
  var a = document.getElementById("chon").value;

  switch (a) {
    case "B":
      document.getElementById("result").innerText = `Xin chào Bố`;
      break;
    case "M":
      document.getElementById("result").innerText = `Xin chào Mẹ`;
      break;
    case "A":
      document.getElementById("result").innerText = `Xin chào Anh trai`;
      break;
    case "E":
      document.getElementById("result").innerText = `Xin chào Em gái`;
      break;
    default:
      document.getElementById("result").innerText = `Xin chào Người lạ `;
      break;
  }
}
//input:  thành viên được trong option
//các bước thực hiện:
//  b1:khai báo biến a tương ứng với giá tri của thành viên mà người dùng đã chọn
//  b2:dùng swich case  để xét điều kiện
//  b3:ta cho a có giá trị tương ứng với case bằng "B" thì in ra câu chào Xin chào Bố
//  b4:ta cho a có giá trị tương ứng với case bằng "M" thì in ra câu chào Xin chào Mẹ
//  b5:ta cho a có giá trị tương ứng với case bằng "A" thì in ra câu chào Xin chào Anh trai
//  b6:ta cho a có giá trị tương ứng với case bằng "E" thì in ra câu chào Xin chào Em gái
//  b7:ta cho giá trị in ra ở default là Xin chào Người lạ nếu như người dùng ko chọn thành viên
//output: câu chào tương ứng với thành viên đc chọn
